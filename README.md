* Build and run the console app as is to create the database.
* Insert a row in the Blogs table in the database
* Change the Blog class so that the Name property is an int instead of a string.
* Debug the application

You should get a DataReader error resulting from the .Rollback() call instead of the actual error that occurs when you call .ToList() on Blogs