﻿using System;

namespace ConsoleApp
{
  public class Blog
  {
    public Guid Id { get; set; }

    public string Description { get; set; }

    public string Name { get; set; }
  }
}
