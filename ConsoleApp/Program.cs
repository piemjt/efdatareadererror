﻿using System;
using System.Linq;

namespace ConsoleApp
{
  class Program
  {
    static void Main(string[] args)
    {
      using (var dbContext = new MyDbContext())
      {
        dbContext.Database.EnsureCreated();

        using (var transaction = dbContext.Database.BeginTransaction())
        {
          try
          {
            var blogs = dbContext.Blogs
              .ToList();

            dbContext.SaveChanges();

            transaction.Commit();
          }

          catch (Exception ex)
          {
            transaction.Rollback();
          }
        }
      }
    }
  }
}
