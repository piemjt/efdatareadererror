﻿using Microsoft.EntityFrameworkCore;

namespace ConsoleApp
{
  public class MyDbContext : DbContext
  {
    public DbSet<Blog> Blogs { get; set; }

    protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
    {
      optionsBuilder.UseSqlServer("Server=(localdb)\\mssqllocaldb;Database=MyBlogs;Trusted_Connection=True");
      //optionsBuilder.UseSqlServer("Server=(localdb)\\mssqllocaldb;Database=MyBlogs;Trusted_Connection=True;MultipleActiveResultSets=true");
    }

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
      modelBuilder.Entity<Blog>();
    }
  }
}
